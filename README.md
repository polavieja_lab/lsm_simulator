A gimmicky light-sheet microscopy simulator. I made this to better understand the relationship between the point-spread function, bleaching, resolution, sheet-depth, and other variables.

It's built on the publicly-available data here: https://janelia.figshare.com/articles/dataset/Whole-brain_light-sheet_imaging_data/7272617

If you use this, please cite the real heroes:
> X. Chen et al., ‘Brain-wide Organization of Neuronal Activity and Convergent Sensorimotor Transformations in Larval Zebrafish’, Neuron, vol. 100, no. 4, pp. 876-890.e5, Nov. 2018, doi: 10.1016/j.neuron.2018.09.042.
